from dash import Dash
from creds.auth_user_password_pairs import VALID_USERNAME_PASSWORD_PAIRS
import dash_auth
from views.layouts import EmsLayout
# from views.lay_message import lay_message
external_stylesheets = ['https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css']

app = Dash(__name__, external_stylesheets=external_stylesheets)
auth = dash_auth.BasicAuth(
    app,
    VALID_USERNAME_PASSWORD_PAIRS
)
app.title = 'ems'
server = app.server
app.config.suppress_callback_exceptions = True
app.layout = EmsLayout().get_app_layout()

