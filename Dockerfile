FROM python:3.7-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV FLASK_APP "/app/app.py"
ENV FLASK_ENV "development"
ENV FLASK_DEBUG True

# update image packages
RUN apt-get update -y && \
    pip install --upgrade pip

# create app directory
RUN mkdir /app

WORKDIR /app

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt

# install requirements
RUN pip install -r requirements.txt

ADD . /app

EXPOSE 5000

ENTRYPOINT [ "python3" ]

CMD [ "run_server.py" ]