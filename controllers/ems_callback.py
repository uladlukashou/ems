from dash.dependencies import Output, Input, State
from services.EmsCalculator import EmsCalculator
from app import app


@app.callback(Output('output-data-upload', 'children'),
              Input('upload-data', 'contents'),
              State('upload-data', 'filename'),
              State('upload-data', 'last_modified'),
              State('e_ref_input', 'value'))
def update_output(list_of_contents: list, list_of_names: list, list_of_dates: list, e_ref: float):
    if list_of_contents is not None:
        return EmsCalculator(list_of_contents, list_of_names, e_ref).get_prepared_df()
