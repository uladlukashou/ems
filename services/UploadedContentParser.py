import base64
import io
from views.exceptions_layouts import get_default_error_message
from pandas import read_excel, read_csv


class UploadedContentParser:
    @staticmethod
    def parse_contents(contents, filename):
        content_type, content_string = contents.split(',')

        decoded = base64.b64decode(content_string)
        try:
            if 'csv' in filename:
                df = read_csv(
                    io.StringIO(decoded.decode('utf-8')), sep=";", header=None)
            elif 'xls' in filename:
                df = read_excel(io.BytesIO(decoded))
        except Exception as e:
            print(e)
            return get_default_error_message('Parsing error')

        return df
