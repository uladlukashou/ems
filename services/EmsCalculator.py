from pandas import DataFrame, concat
import numpy as np
from components.default_data_table import default_data_table
from services.UploadedContentParser import UploadedContentParser


class EmsCalculator:

    def __init__(self, list_of_contents: list, list_of_names: list, e_ref: float):
        self.list_of_contents = list_of_contents
        self.list_of_names = list_of_names
        self.e_ref = e_ref

    @property
    def children(self):
        return [
            UploadedContentParser().parse_contents(c, n) for c, n in
            zip(self.list_of_contents, self.list_of_names)]

    def generate_numeric_concated_df(self) -> DataFrame:

        numbers_from_list_of_names = \
            [((num.split("_")[-1]).split("N")[-1]).split(".csv")[0] for num in self.list_of_names]
        named_list_of_series = [(self.children[i][3]).to_frame(f'N{j}') for i, j in zip(range(len(self.children)),
                                                                                        numbers_from_list_of_names)]
        first_named_df = named_list_of_series.pop(0)
        for named_df in named_list_of_series:
            first_named_df = first_named_df.join(named_df)
        df = first_named_df
        try:
            df = df.apply(lambda x: x.str.replace(',', '.').astype(float), axis=1)
        except AttributeError:
            pass
        df = 20 * np.log10(df / self.e_ref)
        return df

    def calculate_y_field(self) -> DataFrame:

        appended_list = []
        for index, row in self.generate_numeric_concated_df().iterrows():
            row_list = row.tolist()
            sorted_list = row_list.copy()
            sorted_list.sort()
            row['min'] = sorted_list[0]
            counter = 1
            for value in sorted_list:

                count = sum(map(lambda x: (x >= value) & (x <= value + 6), row_list))
                row['count'] = count
                sorted_list_value_index = sorted_list.index(value)
                if count >= 12:
                    row['appropriate_index'] = sorted_list_value_index
                    row['appropriate_min'] = sorted_list[sorted_list.index(value)]
                    appended_list.append(row.to_frame().T)

                    break
                else:

                    if counter == len(sorted_list):
                        row['operation'] = 'FAILED'
                        row['appropriate_index'] = 'FAILED'
                        row['appropriate_min'] = None
                        appended_list.append(row.to_frame().T)
                    else:
                        counter += 1
                        continue

        df_ = concat(appended_list)
        df_ = df_.join((self.children[0][1]).to_frame('B'))
        try:
            df_['B'] = df_['B'].str.replace(",", ".")
        except AttributeError:
            pass

        df_['Y'] = df_['B'].astype(float) - df_['appropriate_min'].astype(float)
        df_['coincide_min?'] = df_['min'] == df_['appropriate_min']
        return df_

    def get_prepared_df(self):
        prep_df = self.calculate_y_field()
        prep_df = prep_df.round(3)
        return default_data_table(prep_df)
