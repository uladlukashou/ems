from dash_table import DataTable
from pandas import DataFrame
from dash_html_components import Div


def default_data_table(df: DataFrame) -> DataTable:
    return Div([DataTable(
        data=df.to_dict('records'),
        columns=[{'name': i, 'id': i} for i in df.columns],
        export_format='xlsx',
        sort_action='native',
        # fixed_rows={'headers': True, 'data': 0},
        style_table={'overflowX': 'auto', 'overflowY': 'auto',
                     'height': '500px',
                     },
        style_header={
            'backgroundColor': 'rgb(230, 230, 230)',
            'fontWeight': 'bold'
        },
        style_cell={
            'width': '{}%'.format(len(df.columns)),
            'textOverflow': 'ellipsis',
            'overflow': 'hidden',
            'border': '1px solid grey'

        },



    )],

    )
