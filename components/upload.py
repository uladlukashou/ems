from dash_core_components import Upload
from dash_html_components import Div


def get_upload(component_id: str, label: str, multi: bool) -> Upload:
    return Upload(
        id=component_id,
        children=Div([label]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        multiple=multi
    )
