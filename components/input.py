from dash_html_components import Div, Label, Br
from dash_core_components import Input


def number_input(component_id: str, value: float, label: str, placeholder: str) -> Div:
    return Div([
        Label(label),
        Br(),

        Input(
            id=component_id,
            type='number',
            value=value,
            placeholder=placeholder,
            min=0.01,

        )
    ])
