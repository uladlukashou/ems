from dash_html_components import Div


def get_default_error_message(message: str):
    return Div([
                message
            ])