from components.upload import get_upload
from components.input import number_input
from dash_html_components import Div


class EmsLayout:

    @staticmethod
    def get_app_layout():
        return Div([
            get_upload(component_id='upload-data',
                       label='Drag or Select', multi=True),
            Div([], className='dropdown-divider mt-2 mb-3'),
            number_input(component_id='e_ref_input',
                         value=20, label='Eref  ',
                         placeholder='input eref'),
            Div([], className='dropdown-divider mt-2 mb-3'),
            Div(id='output-data-upload')

        ],

        )
